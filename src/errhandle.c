#include "errhandle.h"

void
report_err(Err err_type, int row, int doexit, Env *env){

	switch (err_type){
			case TokenErr:
				puts("err");
				DEF_LOG_ERR_NORM("Unknown symbol");
				break;

			case IdenLenErr:
				DEF_LOG_ERR_NORM("Variable name too long");
				break;

			default:
					fprintf(stderr, "IMPOSSIBLE!\n");
					exit(EXIT_FAILURE);
	}

	if (doexit == 1)
			exit(EXIT_FAILURE);
	else if (doexit == 2) // then decide according to the mode
			if (env != NULL && env->execenv.mode == 1) // -c , -s or file
					exit(EXIT_FAILURE);
			
}
