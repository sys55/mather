#pragma once

#include <stdio.h>


#include "env.h"
#include "errhandle.h"
#include "lexer.h"
#include "parser.h"

#define MAX_LINE   BUFSIZ


int 
deal(char *line, int size, Env *env); // returns 1 in the case of an error and 2 in an exit (call to exit())


int
getline_(char *arr, int limit, FILE *input);
