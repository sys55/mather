#pragma once

#include "mathee.h"
#include "lexer.h"

/*
 * TODO: 
 * implement unions below with a new data structure so that no redundant space is used for example when constants are grouped under struct Constant in the mathee.h.
*/


/* #define MAX_ARR 4294967296 // 2 ^ 32 */
/* #define MAX_PARAM 256 // 2 ^ 8 */

	
typedef const char* Name;

typedef enum{
		EXPR_ARTH,
		EXPR_ASSG_ARTH,
		EXPR_ASSG_CONST_NUM,
		EXPR_INPUT_REQ,
}EXPR;

typedef struct{
	Opt_t opt;
	void *operands[2];
}Expr_Arth;

/* typedef struct{ */
/* 	Token_t opt; */
/* 	void *operands[2]; */
/* }Bool_expr; */

/* typedef struct{ */
/* 	Bool_expr condition; */
/* 	Token expr[];  // since the input is evaluated line by line, there is a huge benefit in not parsing the expression before the condition is absouletly true */
/* }Cond_expr; */

/* typedef struct{ */
/* 	Bool_expr condition; */
/* 	Expr expr; */
/* }Set_expr; */

/* typedef union{ */
/* 	Arthm_expr arthm; */
/* 	Bool_expr  boolean; */
/* 	Cond_expr  cond; */
/* 	Set_expr   set; */
/* }Expr; */


/* typedef union{ */
/* 	int arrNum[MAX_ARR]; */
/* 	double arrFlt[MAX_ARR]; */
/* 	// support for arrays of relations (bağıntı) and other types of arrays in maths */	
/* }Arr; */

/* typedef struct{ */
/* 	Name name; */
/* 	Arr cont; */
/* }Set; // TODO: classical functions for sets (s(), subset counter etc) */

/* typedef struct{ */
/* 	Name name; */
/* 	Constant params[MAX_PARAM]; */
/* 	const char *expr; */
/* }Function; */


typedef struct{
	Name var_name;
	Num num;
}Expr_Assg_Const_Num; // a separate struct for easily creating bytecode for using constants faster, in the future?

typedef struct{
	Name var_name;
	Expr_Arth *arth;
}Expr_Assg_Arth;

typedef Name Expr_Input_Req;


typedef struct{
	EXPR type;
	void *expr; // to save space, unions not used in here
}Sentence;


typedef Sentence Root_t, Node_t;

void 
print_ast(Root_t *tree, int size);

int
parse(Token tokens[], int size, Root_t *target);
