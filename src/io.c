#include "io.h"


int 
deal(char *line, int size, Env *env){ // returns 1 in the case of an error and 2 in an exit (call to exit())
	int count;
    Token tokens[MAX_LINE]; 
	Root_t *target = malloc(sizeof(Root_t));

	goto norm;

err:
	free(target);
	return 1;

norm:
	env->execenv.linenum++; // initialized with 0

	if (((count = get_tokens(line, size, env, tokens)) < 0))
		goto err;

	if (parse(tokens, count, target) < 0)
		goto err;



	free(target);
    return 0;
}

int
getline_(char *arr, int limit, FILE *input){
    char c, *line = arr;
	if (limit == 0)
			limit = BUFSIZ;

    while ((c = fgetc(input)) != '\n' && c != EOF && line - arr < limit) *line++ = c;  // TODO: error when the limit is exceeded (where to implement this)
	
	return line - arr;
}
