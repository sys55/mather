#pragma once

#include <stdio.h>
#include <stdlib.h>

#include "env.h"

#define LOG_ERR_NORM(MSG, LINE_NUM, ROW, F_NAME) fprintf(stderr, MSG" ""at line %d, column %d, file %s", LINE_NUM, ROW, F_NAME) // normal errors are the ones caused by an error in the input  
#define DEF_LOG_ERR_NORM(MSG) LOG_ERR_NORM(MSG, env->execenv.linenum, row, env->execenv.filename) // an abstraction formed with the current names of the variables for the LOG_ERR_NOM


typedef enum{
	TokenErr,
	IdenLenErr,
}Err;

void
report_err(Err err_type, int row, int doexit, Env *env);
