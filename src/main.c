#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#include "mathee.h"
#include "env.h"
#include "errhandle.h"
#include "lexer.h"
#include "parser.h"
#include "io.h"
#include "mode.h"


void
usage(void){
    // TODO: A more friendly usage string
    puts("Usage:\n\tmather [arguments]                              run an interactive shell\n\tmather [arguments] filename          run the file\n\tmather [arguments] -c command        run the command and exit\n\tmather [arguments] -s           read the code from standart input\n\nArguments:\n\t-p use reverse polish notation in arithmethic expressions\n\t-v display version and exit\n\t-c run the command\n\t-s read the code from standart input\n\t-b translate to byte-code then run\n");
}

void 
pexit(const char* msg, int excode){
    fwrite(msg, sizeof(char), strlen(msg), stderr);
    fwrite("\n", sizeof(char), 1, stderr);
    exit(excode);
}

int 
main(int argc, char **argv){
    if (argc == 1){
		/* Env env = createnv(); */
		Env env;
        intshell(DEF_PROMPT_IN, DEF_PROMPT_OUT, &env);

		return 0;
	}else
		usage(), pexit("No other mode is supported yet!", EXIT_FAILURE);

}
