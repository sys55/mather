/*
 * TODO: interpretation of negative and floating point numbers 
 * right in the lexer, the program doesn't support these yet.
 */

#include "lexer.h"

/*
 * The stupid comments below are for myself to understand
 * that the code works fine, since I have problems when it
 * comes to dealing with index vs lengths, adding and
 * subtracting ones etc.
 */

int
dealIdentifier(char *cont, int max_char, int max_dest_len, TokenCont *tc){ 
	
	int index; 
	for (index = 0; index < max_char && isalpha(cont[index]); index++);  // index < max_char is checked first because we don't want to access a memory part that we mustn't
	
	if (index > max_dest_len)
		return -1;

	strncpy(tc->id, cont, index); // since index is incremented once more before exiting the loop, in it we have the number of characters 

	if ((tc->id)[index - 1] != '\0') 
			tc->id[index] = '\0'; // strncpy doesn't add a null byte if the src has no null byte in its first index bytes

	return index; // how many characters we have advanced

}

int
dealDigit(char *cont, int limit, TokenCont *tc, Token_t *tt){
	
	int index; 
	int num;
	/* int coeff; */
	/* double flt = .0; */

	*tt = INT;

	for (num = index = 0; index < limit && isdigit(cont[index]); index++)
			num = num * 10 + TO_INT(cont[index]);
	

	/* if (cont[index] == ','){ // index is currently on ',' or some other non-digit character */
	/* 	for (coeff = 1, index++; index < limit && isdigit(cont[index]); index++, coeff++) */
/* //				flt += TO_INT(cont[index]) / (pow(10, coeff)); */
	/* 	*tt = FLOAT; */
	/* } */

	/* if (*tt == FLOAT) */
	/* 	tc->flt = num + flt; */
	/* else */
	/* 	tc->num = num; */
	tc->num = num;

	return index; 

}

void
printTokens(Token tokens[], int count){
		Token *ptr;

		for (ptr = tokens; ptr - tokens < count; ptr++)
				switch (ptr->type){
					case INT:
						printf("\nToken: %d, content: %I64lld\n", ptr->type, ptr->content.num);
						break;

					default:
						printf("\nToken: %d, content: %s\n", ptr->type, ptr->content.opt);
						break;
				}
}

int 
skip_space(char *string){
	int ptr = 0;
	while (isspace(*(string + ptr++)));

	return ptr - 1; // number of advances to be performed in order to pass all spaces, or, the number of spaces

}

int // -1 when an error is occurred
get_tokens(char line[], int size, Env *env, Token tokens[]){
	int ptr = 0;
	int count = 0;

	TokenCont tc;
	Token_t tt;
	Token *pt;


	

	// ptr is an index and of course starts from 0. so we use < here. and the value returned by skip_space is the number of spaces before the first processable character (note that ptr is already incremented before that)
	for (ptr = skip_space(line); ptr < size; ptr++, ptr += skip_space(line + ptr)){

		// TODO: move this read function maybe?

		if (line[ptr] == ';') break;

		pt = &tokens[count++]; 
		pt->col = ptr + 1;

		switch (line[ptr]){

				case '.': 
					DEAL_OPT('.', POINT, tc, tt);
					break;

				case '=': 
					if (line[++ptr] == '>')
						strcpy(tc.opt, "=>"), tt = IF;
					else if (line[ptr] == '=')
						strcpy(tc.opt, "=="), tt = IS_EQUAL;
					else
						DEAL_OPT('=', EQUAL, tc, tt), ptr--;

					break;

				case '|': 
					DEAL_OPT('|', OR, tc, tt);
					break;

				case '?': 
					DEAL_OPT('?', QUE_MARK, tc, tt);
					break;

				case '+': 
					DEAL_OPT('+', PLUS, tc, tt);
					break;

				case '-': 
					if (line[++ptr] == '>')
						strcpy(tc.opt, "->"), tt = ARROW;
					else
							DEAL_OPT('-', MINUS, tc, tt), ptr--;
					break;

				case '*': 
					DEAL_OPT('*', MULT, tc, tt);
					break;

				case '/': 
					DEAL_OPT('/', DIV, tc, tt);
					break;

				case '^': 
					DEAL_OPT('^', POW, tc, tt);
					break;

				case '%': 
					DEAL_OPT('%', PERCENT, tc, tt);
					break;

				case '!': 
					DEAL_OPT('!', EXC_MARK, tc, tt);
					break;

				case ',': 
					DEAL_OPT(',', COMMA, tc, tt);
					break;

				case '>': 
					if (line[++ptr] == '=')
						strcpy(tc.opt, ">="), tt = GREATER_EQ;
					else
							DEAL_OPT('>', GREATER, tc, tt), ptr--;

					break;

				case '<': 
					if (line[++ptr] == '=')
						strcpy(tc.opt, "<="), tt = SMALLER_EQ;
					else
							DEAL_OPT('<', SMALLER, tc, tt), ptr--;

					break;

				case ':': 
					DEAL_OPT(':', COLON, tc, tt);
					break;

				case '(': 
					DEAL_OPT('(', OP_PARAN, tc, tt);
					break;

				case ')': 
					DEAL_OPT(')', CL_PARAN, tc, tt);
					break;


				default:
					if (isalpha(line[ptr]))
							// 														   for the null byte
							if (ptr == ((ptr += dealIdentifier(&line[ptr], size - ptr, MAX_IDEN_LEN - 1, &tc) - 1) + 2)) // wat? xd
								report_err(IdenLenErr, ptr + 1, 2, env);

							else
								tt = IDENT;

					else if (isdigit(line[ptr]))
							ptr += dealDigit(&line[ptr], size - ptr, &tc, &tt) - 1; 

					else{
							report_err(TokenErr, ptr + 1, 2, env);
							return -1;
					}

					break;
		}


		pt->type = tt;
		pt->content = tc;
		pt->row = env->execenv.linenum;

	}

	printTokens(tokens, count);


	return count;


}
