#pragma once

#include <stdint.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <stdint.h>

#include "env.h"
#include "errhandle.h"

#define CPY_OPT(OPT, VARNAME) *VARNAME.opt = (OPT), *(VARNAME.opt + 1) = '\0'  // copy one-char operator
#define ASG_TOKEN(TOKEN, VARNAME) (VARNAME = TOKEN) // assign one-char token
#define DEAL_OPT(OPT, TOKEN, VARNAME_O, VARNAME_T) CPY_OPT(OPT, VARNAME_O), ASG_TOKEN(TOKEN, VARNAME_T)  // deal one-char operator
#define DEAL_OPT_DEF(OPT, TOKEN) DEAL_OPT(OPT, TOKEN, tc, tt) // default names of the variables

#define TO_INT(X) (X - '0')

// #define TOK_VAL(X) ((X).content.opt

#define MAX_IDEN_LEN 256 + 1 // for the null byte


typedef enum{
	POINT,
	IF,
	OR,
	AND,
	QUE_MARK,
	PLUS ,
	MINUS,
	MULT,
	DIV,
	POW,
	PERCENT,
	EXC_MARK,
	COMMA,
	GREATER,
	SMALLER,
	SMALLER_EQ,
	GREATER_EQ,
	EQUAL   ,
	IS_EQUAL,
	COLON,
	ARROW,
	OP_PARAN,
	CL_PARAN,
	IDENT, // identifier
	INT,
	FLOAT,
}Token_t;

typedef union{
	double flt; // float
	long long int num;
	char id[MAX_IDEN_LEN]; // identifier
	char opt[3]; // operator, last element for \0
}TokenCont;

typedef struct{
    Token_t type;
    TokenCont content;
	int col, row;
}Token;

void
printTokens(Token tokens[], int count);

int
dealIdentifier(char *line, int limit, int limit_dest, TokenCont *tc);

int
dealDigit(char *line, int limit, TokenCont *tc, Token_t *tt);

int 
skip_space(char *string);

int // returns 1 in the case of an error
get_tokens(char line[], int size, Env *env, Token tokens[]); 
