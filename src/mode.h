#pragma once

#include "env.h"
#include "io.h"


// TODO: These can be better written with the help of good-looking mathematical characters in unicode
#define DEF_PROMPT_IN  "x    = " 
#define DEF_PROMPT_OUT "f(x) = "

void
intshell(const char *in_prompt, const char *out_prompt, Env *env);


void
ffile(const char *path, Env *env);
