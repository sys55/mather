#pragma once

#include <math.h>

/*
 * Integers are always stored in long long int and floats in double 
 * to not having to complicate things with checking whether to allocate
 * new memory each time an operation is performed
 */


// real
#define INT_R (0)
#define FLT_R (1)

// complex
#define INT_C (0 << 1)
#define FLT_C (1 << 1)

typedef enum{
	ADD,
	SUBTRACT,
	MULTIPLY,
	DIVIDE,
}Opt_t;


typedef struct{
	union{
		long long int int_real;
		double flt_real;
	};
	union{
		long long int int_complex;
		double flt_complex;
	};
}Const_Num; 

typedef struct{
	int num_t : 2; // first bit for the real part, second complex
	Const_Num num;
}Num;

Num
operation(Num *nums, Opt_t opt); 
