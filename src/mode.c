#include "mode.h"


// TODO: place every mode in here

void
intshell(const char *in_prompt, const char *out_prompt, Env *env){ 
    char line[MAX_LINE]; 
	int size;
    
    do{
        /* fputs(prompt, stdin); */ 
		/* fflush(stdin); */
		printf("\n%s", in_prompt);
        size = getline_(line, sizeof(line), stdin); // TODO: arrow keys navigate between inputs.
		printf("%s", out_prompt);
    }while(deal(line, size, env) != 2); // print error then continue and only exit when the exit() function is called
}

void
ffile(const char *path, Env *env){
	FILE *file;
	int size;
	char line[MAX_LINE];

	file = fopen(path, "r");
	
    do{
        size = getline_(line, sizeof(line), file); 
    }while(size != 0 && !deal(line, size, env)); 


}
