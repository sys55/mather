* What is that?  
    Mather is an interpreter that is able to parse an input of a (almost) fully mathmathical syntax. It is not a programming language but rather a program that allows number manipulation (either real or complex) with a syntax close to native maths as much as possible.    
	
	It is a dynamically typed (there exist only 5 types (sets, functions, whole, complex and floating numbers) anyway) language in accordence with the nature of maths.  

	One interesting aspect of the interpreter is no type casts allowed since in mathmathics, when you pass parameters to functions they are only valid if you do so according to the domain set.  

	Variable names can only consist of letters. Unicode characters are not supported yet.  

* Goals  
 []  Compilation with llvm  
 []  Turing complete (I don't know if this is even possible)  
 [] 	A language server   
 []  Unicode support  
 []  Number range as long as the ram goes  


* TODO  
 ()  A better error reporting mechanism, maybe inspired by maths?  
 ()  A function for printing the truth table of a boolean function.  
 ()  A function for plotting a graph.  
 () Complex number support.
